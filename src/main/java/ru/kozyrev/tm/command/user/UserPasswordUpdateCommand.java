package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.exception.user.UserPasswordMatchException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;
import ru.kozyrev.tm.exception.user.UserUpdateFailException;
import ru.kozyrev.tm.util.HashUtil;

public final class UserPasswordUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 12;

    public UserPasswordUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-pass-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for updating password";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();

        System.out.println("[UPDATE PASSWORD]");

        System.out.println("ENTER CURRENT PASSWORD:");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        if (password.isEmpty()) {
            throw new UserPasswordEmptyException();
        }
        @NotNull final String hashPassword = HashUtil.getHash(password);
        final boolean isPasswordTrue = serviceLocator.getUserService().isPasswordTrue(user.getLogin(), hashPassword);
        if (!isPasswordTrue) {
            throw new UserPasswordWrongException();
        }

        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPassword = serviceLocator.getTerminalService().nextLine();
        if (newPassword.isEmpty()) {
            throw new UserPasswordEmptyException();
        }

        System.out.println("RE-ENTER PASSWORD:");
        @NotNull final String reNewPassword = serviceLocator.getTerminalService().nextLine();
        if (reNewPassword.isEmpty()) {
            throw new UserPasswordEmptyException();
        }

        if (!newPassword.equals(reNewPassword)) {
            throw new UserPasswordMatchException();
        }

        user.setPasswordHash(HashUtil.getHash(reNewPassword));
        if (serviceLocator.getUserService().merge(user) != null) {
            System.out.println("[OK]");
            serviceLocator.getStateService().setCurrentUser(user);
        } else {
            throw new UserUpdateFailException();
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
