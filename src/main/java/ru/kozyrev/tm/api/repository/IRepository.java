package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {
    @Nullable
    T findOne(@NotNull String id);

    @NotNull
    List<T> findAll();

    @NotNull
    T persist(@NotNull T entity) throws Exception;

    @NotNull
    T merge(@NotNull T entity);

    @Nullable
    T remove(@NotNull String id);

    void removeAll();
}
