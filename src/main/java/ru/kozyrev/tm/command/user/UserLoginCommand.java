package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.exception.user.UserPasswordWrongException;
import ru.kozyrev.tm.util.HashUtil;

public final class UserLoginCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 3;

    public UserLoginCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for logging in system.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LOGIN]");

        System.out.println("ENTER LOGIN:");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();

        @Nullable final User user = serviceLocator.getUserService().getUserByLogin(login);

        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        if (password.isEmpty()) {
            throw new UserPasswordEmptyException();
        }
        @NotNull final String hashPassword = HashUtil.getHash(password);
        final boolean isPasswordTrue = serviceLocator.getUserService().isPasswordTrue(login, hashPassword);
        if (!isPasswordTrue) {
            throw new UserPasswordWrongException();
        }
        serviceLocator.getStateService().setCurrentUser(user);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
