package ru.kozyrev.tm.command.data.bin;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.dto.DataTransferObject;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.util.PathUtil;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class DataBinSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 60;

    public DataBinSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-bin";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data in Binary format to file";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SERIALIZATION BIN SAVE]");

        DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.JAVA, DataFormat.BIN);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);
        @NotNull final ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(dto.getProjects());
        oos.writeObject(dto.getTasks());
        oos.writeObject(dto.getUsers());

        oos.close();
        fos.close();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
