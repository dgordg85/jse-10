package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {
    @Nullable
    User findOne(@NotNull String id);

    @NotNull
    List<User> findAll();

    @NotNull
    User persist(@NotNull User entity) throws Exception;

    @NotNull
    User merge(@NotNull User entity);

    @Nullable
    User remove(@NotNull String id);

    void removeAll();

    @Nullable
    User getUserByLogin(@NotNull String login);
}