package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IUserRepository;
import ru.kozyrev.tm.api.service.IUserService;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginNotRegistryException;
import ru.kozyrev.tm.exception.user.UserLoginTakenException;
import ru.kozyrev.tm.exception.user.UserPasswordEmptyException;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.util.HashUtil;

@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {
    @NotNull
    private final IUserRepository userRepository = (UserRepository) abstractRepository;

    public UserService(@NotNull final UserRepository userRepository) {
        super(userRepository);
    }

    @NotNull
    @Override
    public final User persist(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EmptyEntityException();
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (getUserByLogin(user.getLogin()) != null) {
            throw new UserLoginTakenException();
        }
        @Nullable final String userPassword = user.getPasswordHash();
        if (userPassword == null || userPassword.isEmpty() || userPassword.equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }
        if (user.getRoleType() == null) {
            user.setRoleType(RoleType.USER);
        }
        return userRepository.persist(user);
    }

    @Nullable
    @Override
    public final User merge(@Nullable final User user) throws Exception {
        if (user == null) {
            throw new EmptyEntityException();
        }
        if (user.getLogin() == null) {
            throw new UserLoginEmptyException();
        }
        if (user.getPasswordHash() == null) {
            throw new UserPasswordEmptyException();
        }
        @Nullable final User userUpdate = userRepository.findOne(user.getId());
        if (userUpdate == null) {
            return null;
        }
        if (!user.getLogin().isEmpty()) {
            userUpdate.setLogin(user.getLogin());
        }
        if (user.getPasswordHash().isEmpty() || user.getPasswordHash().equals(HashUtil.EMPTY_PASSWORD)) {
            throw new UserPasswordEmptyException();
        }
        userUpdate.setPasswordHash(user.getPasswordHash());
        userUpdate.setRoleType(user.getRoleType());
        return userRepository.merge(userUpdate);
    }

    @Nullable
    @Override
    public final User getUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) {
            return null;
        }
        return userRepository.getUserByLogin(login);
    }

    @Override
    public final boolean isPasswordTrue(
            @Nullable final String userLogin,
            @Nullable final String hashPassword
    ) throws Exception {
        if (userLogin == null || userLogin.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (hashPassword == null || hashPassword.isEmpty() || hashPassword.equals(HashUtil.EMPTY_PASSWORD)) {
            return false;
        }
        @Nullable final User user = getUserByLogin(userLogin);
        if (user == null) {
            throw new UserLoginNotRegistryException();
        }
        return hashPassword.equals(user.getPasswordHash());
    }
}
