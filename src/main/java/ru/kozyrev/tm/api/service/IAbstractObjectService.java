package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IAbstractObjectService<T> extends IService<T> {
    @Nullable
    T findOne(@Nullable String id) throws Exception;

    @Nullable
    List<T> findAll() throws Exception;

    @Nullable
    T merge(@Nullable T object) throws Exception;

    @Nullable
    T remove(@Nullable String entityId) throws Exception;

    void removeAll() throws Exception;

    void persist(@Nullable List<T> objects) throws Exception;

    @Nullable
    T findOne(@Nullable String id, @Nullable String userId) throws Exception;

    @Nullable
    List<T> findAll(@Nullable String userId) throws Exception;

    @NotNull
    T persist(@Nullable T object) throws Exception;

    @NotNull
    T persist(@Nullable T object, @Nullable String userId) throws Exception;

    @Nullable
    T merge(@Nullable T object, @Nullable String userId) throws Exception;

    @Nullable
    T remove(@Nullable String entityId, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    String getEntityIdByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    @Nullable
    List<T> findWord(@NotNull String word, @Nullable String userId) throws Exception;

    void persist(@Nullable List<T> objects, @Nullable String userId) throws Exception;
}
