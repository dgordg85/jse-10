package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.user.UserLoginEmptyException;
import ru.kozyrev.tm.exception.user.UserLoginTakenException;
import ru.kozyrev.tm.exception.user.UserUpdateFailException;

public final class UserUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 9;

    public UserUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update user login.";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[UPDATE USER]");

        System.out.println("ENTER NEW LOGIN:");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();

        if (login.isEmpty()) {
            throw new UserLoginEmptyException();
        }
        if (serviceLocator.getUserService().getUserByLogin(login) != null) {
            throw new UserLoginTakenException();
        }

        @NotNull final User user = serviceLocator.getStateService().getCurrentUser();
        user.setLogin(login);

        if (serviceLocator.getUserService().merge(user) != null) {
            System.out.println("[OK]");
            serviceLocator.getStateService().setCurrentUser(user);
        } else {
            throw new UserUpdateFailException();
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
