package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.util.DateUtil;

public final class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 22;

    public ProjectCreateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Create new project.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        @NotNull final Project project = new Project();
        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        project.setName(serviceLocator.getTerminalService().nextLine());

        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(serviceLocator.getTerminalService().nextLine());

        System.out.println("ENTER DATESTART:");
        project.setDateStart(DateUtil.parseDate(serviceLocator.getTerminalService().nextLine()));

        System.out.println("ENTER DATEFINISH:");
        project.setDateFinish(DateUtil.parseDate(serviceLocator.getTerminalService().nextLine()));

        serviceLocator.getProjectService().persist(project, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
