package ru.kozyrev.tm.exception.data;

public class NoDataException extends Exception {
    public NoDataException() {
        super("Can't find save file!");
    }
}
