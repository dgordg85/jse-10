package ru.kozyrev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.exception.enumerated.UnknownTypeException;

@Getter
@RequiredArgsConstructor
public enum Direction {
    ASC("asc"),
    DSC("dsc");

    @NotNull
    private final String displayName;

    static public Direction getType(@Nullable final String pType) throws UnknownTypeException {
        for (Direction type : Direction.values()) {
            if (type.getDisplayName().equals(pType)) {
                return type;
            }
        }
        throw new UnknownTypeException();
    }
}
