package ru.kozyrev.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.dto.DataTransferObject;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.util.PathUtil;

import java.io.FileInputStream;


public class FasterXmlLoadCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 66;

    public FasterXmlLoadCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-load-json-fasterXML";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Load data from JSON file by FasterXML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[FasterXML JSON LOAD]");

        @Nullable final String userId = serviceLocator.getStateService().getCurrentUserId();
        if (userId == null) {
            throw new UserEmptyIdException();
        }
        @NotNull final String path = PathUtil.getPath(SerializeType.FASTER_XML, DataFormat.JSON);
        @NotNull final FileInputStream fis = new FileInputStream(path);

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final DataTransferObject dto = mapper.readValue(fis, DataTransferObject.class);
        dto.unLoad(serviceLocator);

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
