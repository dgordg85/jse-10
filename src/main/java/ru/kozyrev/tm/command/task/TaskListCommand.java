package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.util.PrintUtil;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 40;

    public TaskListCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        System.out.println("INPUT ID PROJECT");
        @NotNull final String projectNum = serviceLocator.getTerminalService().nextLine();

        System.out.println("[TASKS LIST OF PROJECT]");

        @NotNull final String projectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
        @NotNull final Direction direction = serviceLocator.getStateService().getTaskDirection();
        @NotNull final Column column = serviceLocator.getStateService().getTaskColumn();
        @Nullable final List<Task> list = serviceLocator.getTaskService().findAll(projectId, column, direction, userId);

        PrintUtil.printList(list);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
