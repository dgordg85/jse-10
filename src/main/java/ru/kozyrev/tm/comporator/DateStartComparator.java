package ru.kozyrev.tm.comporator;

import ru.kozyrev.tm.entity.AbstractObject;

import java.util.Comparator;
import java.util.Date;

public class DateStartComparator implements Comparator<AbstractObject> {
    public int compare(AbstractObject o1, AbstractObject o2) {
        Date dateStart = o1.getDateStart();
        if (dateStart == null) {
            return 1;
        }
        return o1.getDateStart().compareTo(o2.getDateStart());
    }
}