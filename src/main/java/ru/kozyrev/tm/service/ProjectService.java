package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IProjectRepository;
import ru.kozyrev.tm.api.service.IProjectService;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.exception.command.ServiceFailException;
import ru.kozyrev.tm.exception.entity.DateException;
import ru.kozyrev.tm.exception.entity.DescriptionException;
import ru.kozyrev.tm.exception.entity.EmptyEntityException;
import ru.kozyrev.tm.exception.entity.NameException;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.repository.ProjectRepository;

import java.util.List;

@NoArgsConstructor
public class ProjectService extends AbstractObjectService<Project> implements IProjectService {
    @NotNull
    private final IProjectRepository projectRepository = (ProjectRepository) abstractRepository;

    public ProjectService(@NotNull final ProjectRepository projectRepository) {
        super(projectRepository);
    }

    @NotNull
    @Override
    public final Project persist(@Nullable final Project project) throws Exception {
        if (project == null) {
            throw new EmptyEntityException();
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new NameException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        return projectRepository.persist(project);
    }

    @NotNull
    @Override
    public final Project persist(
            @Nullable final Project project,
            @Nullable final String userId
    ) throws Exception {
        if (project == null) {
            throw new EmptyEntityException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (project.getName() == null || project.getName().isEmpty()) {
            throw new NameException();
        }
        if (project.getDescription() == null || project.getDescription().isEmpty()) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        return projectRepository.persist(project, userId);
    }

    @Nullable
    @Override
    public final Project merge(
            @Nullable final Project project,
            @Nullable final String userId
    ) throws Exception {
        if (project == null) {
            throw new ServiceFailException();
        }
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        if (project.getName() == null) {
            throw new NameException();
        }
        if (project.getDescription() == null) {
            throw new DescriptionException();
        }
        if (project.getDateStart() == null) {
            throw new DateException();
        }
        if (project.getDateFinish() == null) {
            throw new DateException();
        }
        @Nullable final Project projectUpdate = projectRepository.findOne(project.getId(), userId);
        if (projectUpdate == null) {
            return persist(project, userId);
        }
        if (!project.getName().isEmpty()) {
            projectUpdate.setName(project.getName());
        }
        if (!project.getDescription().isEmpty()) {
            projectUpdate.setDescription(project.getDescription());
        }
        projectUpdate.setDateStart(project.getDateStart());
        projectUpdate.setDateFinish(project.getDateFinish());

        if (project.getStatus() != null) {
            projectUpdate.setStatus(project.getStatus());
        }
        return projectRepository.merge(projectUpdate, userId);
    }

    @Nullable
    @Override
    public final List<Project> findAll(
            @NotNull final Column column,
            @NotNull final Direction direction,
            @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        @Nullable final List<Project> list;
        switch (column) {
            case DATE_BEGIN:
                list = projectRepository.findAllByDateBegin(direction, userId);
                break;
            case DATE_FINISH:
                list = projectRepository.findAllByDateFinish(direction, userId);
                break;
            case STATUS:
                list = projectRepository.findAllByStatus(direction, userId);
                break;
            default:
                list = projectRepository.findAllByNum(direction, userId);
        }
        return list;
    }
}
