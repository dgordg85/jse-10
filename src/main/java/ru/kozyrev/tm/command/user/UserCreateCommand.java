package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.user.UserPasswordMatchException;
import ru.kozyrev.tm.util.HashUtil;

public final class UserCreateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 1;

    public UserCreateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "User for registry new user";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final User user = new User();

        System.out.println("[USER REGISTRATION]");

        System.out.println("ENTER LOGIN:");
        user.setLogin(serviceLocator.getTerminalService().nextLine());

        System.out.println("ENTER PASSWORD:");
        final String password = serviceLocator.getTerminalService().nextLine();

        System.out.println("RE-ENTER PASSWORD:");
        final String password2 = serviceLocator.getTerminalService().nextLine();

        if (!password.equals(password2)) {
            throw new UserPasswordMatchException();
        }
        user.setPasswordHash(HashUtil.getHash(password));
        serviceLocator.getUserService().persist(user);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
