package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.RoleType;

public class ProjectSortCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 70;

    public ProjectSortCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "sort-project";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Using for set sorting option for Projects";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SORT PROJECT]");
        System.out.println("'ASC' / 'DSC'?");
        @NotNull final String sortString = serviceLocator.getTerminalService().nextLine().toLowerCase();

        if (!sortString.isEmpty()) {
            @NotNull final Direction direction = Direction.getType(sortString);
            serviceLocator.getStateService().setProjectDirection(direction);
        }

        System.out.println("Column for sorting? (num / dateBegin / dateEnd / Status)");
        @NotNull final String columnStr = serviceLocator.getTerminalService().nextLine().toLowerCase();

        if (!columnStr.isEmpty()) {
            @NotNull Column column = Column.getType(columnStr);
            serviceLocator.getStateService().setProjectColumn(column);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
