package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 46;

    public TaskUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update selected task.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        @NotNull final Task task = new Task();

        System.out.println("[UPDATE TASK]\nENTER ID:");
        @NotNull final String taskNum = serviceLocator.getTerminalService().nextLine();

        @Nullable final String taskId = serviceLocator.getTaskService().getEntityIdByShortLink(taskNum, userId);
        task.setId(taskId);

        System.out.println("NEW NAME:");
        task.setName(serviceLocator.getTerminalService().nextLine());

        System.out.println("NEW PROJECT ID:");
        @NotNull final String projectNum = serviceLocator.getTerminalService().nextLine();

        if (!projectNum.isEmpty()) {
            @NotNull final String projectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
            task.setProjectId(projectId);
        } else {
            task.setProjectId("");
        }

        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(serviceLocator.getTerminalService().nextLine());

        System.out.println("ENTER DATESTART:");
        @NotNull final String dateBegin = serviceLocator.getTerminalService().nextLine();

        if (!dateBegin.isEmpty()) {
            task.setDateStart(DateUtil.parseDate(dateBegin));
        } else {
            task.setDateStart(null);
        }

        System.out.println("ENTER DATEFINISH:");
        @NotNull final String dateFinish = serviceLocator.getTerminalService().nextLine();

        if (!dateFinish.isEmpty()) {
            task.setDateFinish(DateUtil.parseDate(dateFinish));
        } else {
            task.setDateFinish(null);
        }

        System.out.println("STATUS: (plan / progress / ready)");
        @NotNull final String statusString = serviceLocator.getTerminalService().nextLine();
        @Nullable DocumentStatus status = null;
        if (!statusString.isEmpty()) {
            status = DocumentStatus.getType(statusString);
        }
        task.setStatus(status);
        serviceLocator.getTaskService().merge(task, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }

}
