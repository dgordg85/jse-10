package ru.kozyrev.tm.command.data.xml;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.dto.DataTransferObject;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.util.PathUtil;

import java.io.FileInputStream;

public class FasterXmlLoadCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 64;

    public FasterXmlLoadCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-load-xml-fasterXML";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Load data from XML file by FasterXML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[FasterXML XML LOAD]");

        @Nullable final String userId = serviceLocator.getStateService().getCurrentUserId();
        if (userId == null) {
            throw new UserEmptyIdException();
        }
        @NotNull final JacksonXmlModule module = new JacksonXmlModule();
        @NotNull final XmlMapper mapper = new XmlMapper(module);

        @NotNull final String path = PathUtil.getPath(SerializeType.FASTER_XML, DataFormat.XML);
        @NotNull final FileInputStream fis = new FileInputStream(path);

        @NotNull final DataTransferObject dto = mapper.readValue(fis, DataTransferObject.class);
        dto.unLoad(serviceLocator);

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
