package ru.kozyrev.tm.command.data.xml;

import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.dto.DataTransferObject;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.util.PathUtil;

import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class JaxBSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 69;

    public JaxBSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-xml-JaxB";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data from XML file by Jax-B";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[Jax-B XML SAVE]");
        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.JAX_B, DataFormat.XML);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = (JAXBContext) JAXBContext.newInstance(DataTransferObject.class);

        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/xml");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(dto, fos);

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
