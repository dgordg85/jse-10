package ru.kozyrev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.entity.EmptyStringException;
import ru.kozyrev.tm.util.PrintUtil;

import java.util.List;

public class TextFinderCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 95;

    public TextFinderCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "find";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Search words in Project/Tasks";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        System.out.println("[FIND]");
        System.out.println("Print word...");
        @NotNull final String word = serviceLocator.getTerminalService().nextLine();
        if (word.isEmpty()) {
            throw new EmptyStringException();
        }

        @Nullable final List<Project> listProjects = serviceLocator.getProjectService().findWord(word, userId);
        if (listProjects != null) {
            System.out.println("[FIND PROJECTS]");
            PrintUtil.printList(listProjects);
        }

        @Nullable final List<Task> listTasks = serviceLocator.getTaskService().findWord(word, userId);
        if (listTasks != null) {
            System.out.println("[FIND TASKS]");
            PrintUtil.printList(listTasks);
        }

        if (listProjects == null && listTasks == null) {
            System.out.printf("%s - not find!\n", word);
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
