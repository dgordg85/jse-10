package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.enumerated.RoleType;

public final class TaskClearCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 42;

    public TaskClearCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        System.out.println("[CLEAR]");
        System.out.println("[TASK REMOVES]\nENTER PROJECT ID:");
        @NotNull final String projectNum = serviceLocator.getTerminalService().nextLine();

        @NotNull final String projectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
        serviceLocator.getTaskService().removeAll(projectId, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
