package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.util.PrintUtil;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 20;

    public ProjectListCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show all projects.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        System.out.println("[PROJECTS LIST]");
        @NotNull final Direction direction = serviceLocator.getStateService().getProjectDirection();
        @NotNull final Column column = serviceLocator.getStateService().getProjectColumn();
        @Nullable final List<Project> list = serviceLocator.getProjectService().findAll(column, direction, userId);

        PrintUtil.printList(list);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
