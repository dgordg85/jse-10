package ru.kozyrev.tm;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.context.Bootstrap;

public final class Application {
    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
