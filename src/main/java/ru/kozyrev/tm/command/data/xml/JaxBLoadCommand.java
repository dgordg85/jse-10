package ru.kozyrev.tm.command.data.xml;

import org.eclipse.persistence.jaxb.JAXBContext;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.dto.DataTransferObject;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.util.PathUtil;

import javax.xml.bind.Unmarshaller;
import java.io.File;

public class JaxBLoadCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 70;

    public JaxBLoadCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-load-xml-JaxB";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Load data from XML file by Jax-B";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[Jax-B XML LOAD]");

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext context = (JAXBContext) JAXBContext.newInstance(DataTransferObject.class);
        @NotNull final Unmarshaller unMarshaller = context.createUnmarshaller();

        @NotNull final String path = PathUtil.getPath(SerializeType.JAX_B, DataFormat.XML);
        @NotNull final DataTransferObject dto = (DataTransferObject) unMarshaller.unmarshal(new File(path));

        dto.unLoad(serviceLocator);

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
