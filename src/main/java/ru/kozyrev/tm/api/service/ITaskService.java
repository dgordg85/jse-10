package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.List;

public interface ITaskService extends IAbstractObjectService<Task> {
    @Nullable
    Task findOne(@Nullable String id, @Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String userId) throws Exception;

    @NotNull
    Task persist(@Nullable Task object, @Nullable String userId) throws Exception;

    @Nullable
    Task merge(@Nullable Task object, @Nullable String userId) throws Exception;

    @Nullable
    Task remove(@Nullable String entityId, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    String getEntityIdByShortLink(@Nullable String num, @Nullable String userId) throws Exception;

    void removeAll(@Nullable String projectId, @Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String projectId, @Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable String projectId, @NotNull Column column, @NotNull Direction direction, @Nullable String userId) throws Exception;
}
