package ru.kozyrev.tm.command.data.bin;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.exception.data.NoDataException;
import ru.kozyrev.tm.util.CastUtil;
import ru.kozyrev.tm.util.PathUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

public class DataBinLoadCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 62;

    public DataBinLoadCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-load-bin";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Load data from Binary file";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SERIALIZATION BIN LOAD]");

        serviceLocator.getProjectService().removeAll();
        serviceLocator.getTaskService().removeAll();
        serviceLocator.getUserService().removeAll();

        @NotNull final String path = PathUtil.getPath(SerializeType.JAVA, DataFormat.BIN);
        try {
            @NotNull final FileInputStream fis = new FileInputStream(path);
            @NotNull final ObjectInputStream ois = new ObjectInputStream(fis);

            @NotNull final List<Project> projects = CastUtil.toEntity(ois.readObject());
            @NotNull final List<Task> tasks = CastUtil.toEntity(ois.readObject());
            @NotNull final List<User> users = CastUtil.toEntity(ois.readObject());

            serviceLocator.getProjectService().persist(projects);
            serviceLocator.getTaskService().persist(tasks);
            serviceLocator.getUserService().persist(users);

            fis.close();
            ois.close();
        } catch (IOException e) {
            throw new NoDataException();
        }

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
