package ru.kozyrev.tm.exception.command;

public class DataFailException extends Exception {
    public DataFailException() {
        super("No data found!");
    }
}
