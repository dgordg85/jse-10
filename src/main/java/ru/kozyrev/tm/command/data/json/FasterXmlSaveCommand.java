package ru.kozyrev.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.dto.DataTransferObject;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.util.PathUtil;

import java.io.FileOutputStream;

public class FasterXmlSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 65;

    public FasterXmlSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-json-fasterXML";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data in JSON format to file by FasterXML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[FasterXML JSON SAVE]");

        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.FASTER_XML, DataFormat.JSON);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(fos, dto);

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
