package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.enumerated.RoleType;

public final class UserLogoutCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 5;

    public UserLogoutCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for logout.";
    }

    @Override
    public final void execute() {
        serviceLocator.getStateService().clearUser();
        System.out.println("[User logout!]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
