package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IRepository;
import ru.kozyrev.tm.api.service.IService;
import ru.kozyrev.tm.entity.AbstractEntity;
import ru.kozyrev.tm.exception.command.DataFailException;
import ru.kozyrev.tm.exception.data.NoDataException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.repository.AbstractRepository;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {
    @NotNull
    protected IRepository<T> abstractRepository;

    public AbstractService(@NotNull final AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Nullable
    @Override
    public final T findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new IndexException();
        }
        return abstractRepository.findOne(id);
    }

    @NotNull
    @Override
    public final List<T> findAll() {
        return abstractRepository.findAll();
    }

    @NotNull
    @Override
    public T persist(@Nullable final T entity) throws Exception {
        if (entity == null) {
            throw new DataFailException();
        }
        return abstractRepository.persist(entity);
    }

    @Nullable
    @Override
    public T merge(@Nullable final T entity) throws Exception {
        if (entity == null) {
            throw new DataFailException();
        }
        return abstractRepository.merge(entity);
    }

    @Nullable
    @Override
    public final T remove(@Nullable final String entityId) throws Exception {
        if (entityId == null || entityId.isEmpty()) {
            throw new IndexException();
        }
        return abstractRepository.remove(entityId);
    }

    @Override
    public final void removeAll() {
        abstractRepository.removeAll();
    }

    @Override
    public void persist(@Nullable final List<T> objects) throws Exception {
        if (objects == null) {
            throw new NoDataException();
        }
        for (T object : objects) {
            if (object == null) {
                continue;
            }
            persist(object);
        }
    }
}
