package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kozyrev.tm.api.service.ITerminalService;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.command.AccessForbiddenException;
import ru.kozyrev.tm.exception.command.CommandCorruptException;
import ru.kozyrev.tm.exception.command.CommandException;
import ru.kozyrev.tm.exception.command.ServiceFailException;

import java.util.*;

@NoArgsConstructor
public class TerminalService implements ITerminalService {
    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    private ServiceLocator serviceLocator = null;

    @NotNull
    private Scanner sc = new Scanner(System.in);

    public TerminalService(@Nullable ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public final void printCommands() throws Exception {
        for (AbstractCommand command : commands.values()) {
            if (isCommandAllow(command)) {
                System.out.println(command.getName() + ": " + command.getDescription());
            }

        }
    }

    @Override
    public final void registryCommand(@Nullable final AbstractCommand command) throws Exception {
        if (command == null) {
            throw new CommandCorruptException();
        }
        if (command.getName().isEmpty())
            throw new CommandCorruptException();
        if (command.getDescription().isEmpty())
            throw new CommandCorruptException();
        if (serviceLocator == null) {
            throw new ServiceFailException();
        }
        command.setServiceLocator(serviceLocator);
        commands.put(command.getName(), command);
    }

    @Override
    public final void execute(@Nullable final String commandStr) throws Exception {
        if (commandStr == null || commandStr.isEmpty()) {
            throw new CommandException();
        }
        AbstractCommand command = commands.get(commandStr);
        if (!isCommandAllow(command)) {
            throw new AccessForbiddenException();
        }
        command.execute();
    }

    public final boolean isCommandAllow(@Nullable final AbstractCommand command) throws Exception {
        if (serviceLocator == null) {
            throw new ServiceFailException();
        }
        if (command == null) {
            throw new CommandException();
        }
        @Nullable final User currentUser = serviceLocator.getStateService().getCurrentUser();
        if (currentUser == null) {
            return command.getSecure();
        }
        if (command.getSecure()) {
            return true;
        }
        @NotNull final List<RoleType> roleTypes = command.getRoleTypes();
        if (roleTypes.size() == 0) {
            throw new CommandException();
        }
        return roleTypes.contains(currentUser.getRoleType());
    }

    @Override
    public final void initCommands() throws Exception {
        final Set<Class<? extends AbstractCommand>> classes2 =
                new Reflections("ru.kozyrev.tm").getSubTypesOf(AbstractCommand.class);
        for (@NotNull Class<?> clazz : classes2) {
            registryCommand((AbstractCommand) clazz.getDeclaredConstructor().newInstance());
        }
        sortCommand();
    }

    public final void sortCommand() {
        @NotNull final List<Map.Entry<String, AbstractCommand>> entries =
                new ArrayList<>(commands.entrySet());
        entries.sort(Comparator.comparingInt(a -> a.getValue().getSortId()));
        commands.clear();
        for (Map.Entry<String, AbstractCommand> entry : entries) {
            commands.put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public final void welcomeMsg() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    @Override
    public final void errorDateMsg() {
        System.out.println("Wrong date! User dd-MM-YYYY format");
    }

    @NotNull
    @Override
    public final String nextLine() {
        return sc.nextLine();
    }

    @Override
    public final void closeSc() {
        sc.close();
    }

    @Override
    public final void setSc(@NotNull final Scanner sc) {
        this.sc = sc;
    }
}
