package ru.kozyrev.tm.exception.entity;

public class EmptyEntityException extends Exception {
    public EmptyEntityException() {
        super("Empty entity!");
    }
}
