package ru.kozyrev.tm.comporator;

import ru.kozyrev.tm.entity.AbstractObject;

import java.util.Comparator;
import java.util.Date;

public class DateFinishComparator implements Comparator<AbstractObject> {
    public int compare(AbstractObject o1, AbstractObject o2) {
        Date dateFinish = o1.getDateFinish();
        if (dateFinish == null) {
            return 1;
        }
        return o1.getDateFinish().compareTo(o2.getDateFinish());
    }
}
