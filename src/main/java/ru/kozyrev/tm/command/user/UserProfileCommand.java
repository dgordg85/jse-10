package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;

public final class UserProfileCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 7;

    public UserProfileCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Show user profile";
    }

    @Override
    public final void execute() {
        @NotNull final User currentUser = serviceLocator.getStateService().getCurrentUser();

        System.out.println("[USER PROFILE]");
        System.out.printf("Login: %s\n", currentUser.getLogin());
        if (currentUser.getRoleType() != null) {
            System.out.printf("Role: %s\n", currentUser.getRoleType().getDisplayName());
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
