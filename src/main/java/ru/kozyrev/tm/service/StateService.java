package ru.kozyrev.tm.service;


import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.IStateService;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;


@Getter
@Setter
public class StateService implements IStateService {
    @Nullable
    private User currentUser = null;

    @NotNull
    private Column projectColumn = Column.NUM;

    @NotNull
    private Column taskColumn = Column.NUM;

    @NotNull
    private Direction projectDirection = Direction.ASC;

    @NotNull
    private Direction taskDirection = Direction.ASC;

    @Override
    public final void resetState() {
        projectColumn = Column.NUM;
        taskColumn = Column.NUM;
        projectDirection = Direction.ASC;
        taskDirection = Direction.ASC;
    }

    @Override
    public final boolean isUserAuth() {
        return currentUser != null;
    }

    @Nullable
    @Override
    public final String getCurrentUserId() {
        if (currentUser == null) {
            return null;
        }
        return currentUser.getId();
    }

    @Override
    public final void clearUser() {
        currentUser = null;
    }

    @Override
    public final void setCurrentUser(@Nullable final User user) {
        currentUser = user;
    }
}
