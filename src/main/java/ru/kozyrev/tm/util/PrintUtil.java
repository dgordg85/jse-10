package ru.kozyrev.tm.util;

import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.AbstractObject;
import ru.kozyrev.tm.exception.enumerated.UnknownTypeException;

import java.util.List;

public final class PrintUtil {
    public final static <T extends AbstractObject> void printList(@Nullable final List<T> list) throws Exception {
        if (list == null || list.size() == 0) {
            System.out.println("[EMPTY]");
            return;
        }
        int count = 1;

        for (T entity : list) {
            if (entity.getStatus() == null) {
                throw new UnknownTypeException();
            }
            System.out.printf("%d. %s, EDIT ID#%d, %s, s:%s, f:%s - %s\n",
                    count++,
                    entity.getName(),
                    entity.getShortLink(),
                    entity.getDescription(),
                    DateUtil.getDate(entity.getDateStart()),
                    DateUtil.getDate(entity.getDateFinish()),
                    entity.getStatus().getDisplayName()
            );
        }
    }
}
