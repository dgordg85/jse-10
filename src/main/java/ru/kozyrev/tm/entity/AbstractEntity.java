package ru.kozyrev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
public abstract class AbstractEntity implements Serializable {
    private static final long SerialVersionUID = 1L;

    @NotNull
    protected String id = UUID.randomUUID().toString();
}
