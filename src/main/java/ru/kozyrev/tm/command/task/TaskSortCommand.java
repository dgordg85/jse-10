package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.enumerated.Column;
import ru.kozyrev.tm.enumerated.Direction;
import ru.kozyrev.tm.enumerated.RoleType;

public class TaskSortCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 75;

    public TaskSortCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "sort-task";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Using for set sorting option for Tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SORT TASK]");
        System.out.println("'ASC' / 'DSC'?");
        @NotNull final String sortString = serviceLocator.getTerminalService().nextLine().toLowerCase();

        if (!sortString.isEmpty()) {
            @NotNull final Direction direction = Direction.getType(sortString);
            serviceLocator.getStateService().setTaskDirection(direction);
        }

        System.out.println("Column for sorting? (num / dateBegin / dateEnd / Status)");
        @NotNull final String columnStr = serviceLocator.getTerminalService().nextLine().toLowerCase();

        if (!columnStr.isEmpty()) {
            @NotNull final Column column = Column.getType(columnStr);
            serviceLocator.getStateService().setTaskColumn(column);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
