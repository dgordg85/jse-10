package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.enumerated.DocumentStatus;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.util.DateUtil;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 24;

    public ProjectUpdateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Update Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        @NotNull final Project project = new Project();

        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        @NotNull final String projectNum = serviceLocator.getTerminalService().nextLine();

        @NotNull final String newProjectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
        project.setId(newProjectId);

        System.out.println("ENTER NAME:");
        project.setName(serviceLocator.getTerminalService().nextLine());

        System.out.println("ENTER DESCRIPTION:");
        project.setDescription(serviceLocator.getTerminalService().nextLine());

        System.out.println("ENTER DATESTART:");
        project.setDateStart(DateUtil.parseDate(serviceLocator.getTerminalService().nextLine()));

        System.out.println("ENTER DATEFINISH:");
        project.setDateFinish(DateUtil.parseDate(serviceLocator.getTerminalService().nextLine()));

        System.out.println("STATUS: (plan / progress / ready)");
        @NotNull final String statusString = serviceLocator.getTerminalService().nextLine();

        @Nullable DocumentStatus status = null;
        if (!statusString.isEmpty()) {
            status = DocumentStatus.getType(statusString);
        }
        project.setStatus(status);

        serviceLocator.getProjectService().merge(project, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
