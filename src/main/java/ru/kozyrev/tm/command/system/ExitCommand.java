package ru.kozyrev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.enumerated.RoleType;

public final class ExitCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 250;

    public ExitCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        secure = true;
    }

    @NotNull
    @Override
    public final String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Quit from manager.";
    }

    @Override
    public final void execute() {
        serviceLocator.getTerminalService().closeSc();
        System.exit(0);
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
