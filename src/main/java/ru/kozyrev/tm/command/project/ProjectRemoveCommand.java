package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.entity.IndexException;

public final class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 26;

    public ProjectRemoveCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove Selected project.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        System.out.println("[PROJECT REMOVE]\nENTER ID:");
        @NotNull final String projectNum = serviceLocator.getTerminalService().nextLine();

        @NotNull final String projectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
        if (serviceLocator.getProjectService().remove(projectId, userId) == null) {
            throw new IndexException();
        }
        serviceLocator.getTaskService().removeAll(projectId, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
