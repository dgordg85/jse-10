package ru.kozyrev.tm.exception.user;

public final class UserUpdateFailException extends Exception {
    public UserUpdateFailException() {
        super("User update fail!");
    }
}
