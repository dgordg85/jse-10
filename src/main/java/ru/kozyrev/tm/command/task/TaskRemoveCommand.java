package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.enumerated.RoleType;

public final class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 48;

    public TaskRemoveCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        System.out.println("[TASK DELETE]\nENTER ID:");
        @NotNull final String taskNum = serviceLocator.getTerminalService().nextLine();

        @NotNull final String taskId = serviceLocator.getTaskService().getEntityIdByShortLink(taskNum, userId);
        serviceLocator.getTaskService().remove(taskId, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
