package ru.kozyrev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.repository.IAbstractObjectRepository;
import ru.kozyrev.tm.api.service.IAbstractObjectService;
import ru.kozyrev.tm.entity.AbstractObject;
import ru.kozyrev.tm.exception.data.NoDataException;
import ru.kozyrev.tm.exception.entity.IndexException;
import ru.kozyrev.tm.exception.user.UserEmptyIdException;
import ru.kozyrev.tm.repository.AbstractObjectRepository;
import ru.kozyrev.tm.util.StringUtil;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractObjectService<T extends AbstractObject> extends AbstractService<T> implements IAbstractObjectService<T> {
    @NotNull
    protected IAbstractObjectRepository<T> abstractObjectRepository = (IAbstractObjectRepository<T>) abstractRepository;

    public AbstractObjectService(@NotNull AbstractObjectRepository<T> abstractObjectRepository) {
        super(abstractObjectRepository);
    }

    @Nullable
    @Override
    public final T findOne(@Nullable final String id, @Nullable final String userId) throws Exception {
        if (id == null || id.isEmpty()) {
            return null;
        }
        if (userId == null) {
            throw new UserEmptyIdException();
        }
        return abstractObjectRepository.findOne(id, userId);
    }

    @Nullable
    @Override
    public final List<T> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new UserEmptyIdException();
        }
        return abstractObjectRepository.findAll(userId);
    }

    @NotNull
    @Override
    public abstract T persist(@Nullable final T object) throws Exception;

    @NotNull
    @Override
    public abstract T persist(@Nullable final T object, @Nullable final String userId) throws Exception;

    @Nullable
    @Override
    public abstract T merge(@Nullable final T object, @Nullable final String userId) throws Exception;

    @Nullable
    @Override
    public final T remove(@Nullable final String objectId, @Nullable final String userId) throws Exception {
        if (objectId == null || objectId.isEmpty()) {
            return null;
        }
        if (userId == null) {
            throw new UserEmptyIdException();
        }
        return abstractObjectRepository.remove(objectId, userId);
    }

    @Override
    public final void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null) {
            throw new UserEmptyIdException();
        }
        abstractObjectRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public final String getEntityIdByShortLink(@Nullable final String num, @Nullable final String userId) throws Exception {
        if (num == null || num.isEmpty()) {
            throw new IndexException();
        }
        if (userId == null) {
            throw new UserEmptyIdException();
        }
        final int shortLink = StringUtil.parseToInt(num);
        @NotNull final T entity = abstractObjectRepository.findOneByShortLink(shortLink, userId);
        return entity.getId();
    }

    @Nullable
    @Override
    public List<T> findWord(@NotNull final String word, @Nullable final String userId) throws Exception {
        if (userId == null) {
            throw new UserEmptyIdException();
        }
        return abstractObjectRepository.findWord(word, userId);
    }

    @Override
    public void persist(@Nullable final List<T> objects, @Nullable final String userId) throws Exception {
        if (objects == null) {
            throw new NoDataException();
        }
        if (userId == null) {
            throw new UserEmptyIdException();
        }
        for (T object : objects) {
            persist(object, userId);
        }
    }
}
