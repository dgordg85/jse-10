package ru.kozyrev.tm.command;

import lombok.Getter;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.api.service.ServiceLocator;
import ru.kozyrev.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.List;

@Getter
public abstract class AbstractCommand {
    @NonNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    protected ServiceLocator serviceLocator;

    @NotNull
    protected Boolean secure = false;

    public final void setServiceLocator(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract Integer getSortId();

}
