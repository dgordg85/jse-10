package ru.kozyrev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.util.DateUtil;

public final class TaskCreateCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 44;

    public TaskCreateCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Create new tasks.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        @NotNull final Task task = new Task();

        System.out.println("[TASK CREATE]\nENTER NAME:");
        task.setName(serviceLocator.getTerminalService().nextLine());

        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectNum = serviceLocator.getTerminalService().nextLine();

        @NotNull final String projectId = serviceLocator.getProjectService().getEntityIdByShortLink(projectNum, userId);
        task.setProjectId(projectId);

        System.out.println("ENTER DESCRIPTION:");
        task.setDescription(serviceLocator.getTerminalService().nextLine());

        System.out.println("ENTER DATESTART:");
        task.setDateStart(DateUtil.parseDate(serviceLocator.getTerminalService().nextLine()));

        System.out.println("ENTER DATEFINISH:");
        task.setDateFinish(DateUtil.parseDate(serviceLocator.getTerminalService().nextLine()));

        serviceLocator.getTaskService().persist(task, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
