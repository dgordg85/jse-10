package ru.kozyrev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.api.service.*;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.exception.entity.DateException;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.repository.UserRepository;
import ru.kozyrev.tm.service.*;
import ru.kozyrev.tm.util.HashUtil;

import java.text.ParseException;

@Getter
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final IProjectService projectService = new ProjectService(new ProjectRepository());

    @NotNull
    private final ITaskService taskService = new TaskService(new TaskRepository());

    @NotNull
    private final IUserService userService = new UserService(new UserRepository());

    @NotNull
    private final ITerminalService terminalService = new TerminalService(this);

    @NotNull
    private final StateService stateService = new StateService();

    public final void init() throws Exception {
        getTerminalService().initCommands();
        createDefaultUsers();

        getTerminalService().welcomeMsg();
        @Nullable String command;
        while (true) {
            try {
                command = terminalService.nextLine().toLowerCase().replace('_', '-');
                getTerminalService().execute(command);
            } catch (ParseException | DateException e) {
                getTerminalService().errorDateMsg();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public final void createDefaultUsers() throws Exception {
        @NotNull User user = new User();
        user.setLogin("user");
        user.setPasswordHash(HashUtil.getHash("user"));
        user.setRoleType(RoleType.USER);
        userService.persist(user);

        @NotNull User admin = new User();
        admin.setLogin("admin");
        admin.setPasswordHash(HashUtil.getHash("admin"));
        admin.setRoleType(RoleType.ADMIN);
        userService.persist(admin);
    }
}
