package ru.kozyrev.tm.exception.data;

public class LoadException extends Exception {
    public LoadException() {
        super("Load data fail!");
    }
}
