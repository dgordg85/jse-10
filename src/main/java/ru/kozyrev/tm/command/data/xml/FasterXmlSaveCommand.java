package ru.kozyrev.tm.command.data.xml;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.dto.DataTransferObject;
import ru.kozyrev.tm.enumerated.DataFormat;
import ru.kozyrev.tm.enumerated.RoleType;
import ru.kozyrev.tm.enumerated.SerializeType;
import ru.kozyrev.tm.util.PathUtil;

import java.io.FileOutputStream;

public class FasterXmlSaveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 63;

    public FasterXmlSaveCommand() {
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "data-save-xml-fasterXML";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Save data to XML file by FasterXML";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[FasterXML XML SAVE]");

        @NotNull final String path = PathUtil.getPathAndCreateFile(SerializeType.FASTER_XML, DataFormat.XML);
        @NotNull final FileOutputStream fos = new FileOutputStream(path);

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.load(serviceLocator);

        @NotNull final JacksonXmlModule module = new JacksonXmlModule();

        @NotNull final XmlMapper mapper = new XmlMapper(module);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mapper.writeValue(fos, dto);

        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
