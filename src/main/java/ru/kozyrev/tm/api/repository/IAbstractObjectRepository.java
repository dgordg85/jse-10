package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IAbstractObjectRepository<T> extends IRepository<T> {
    @Nullable
    T findOne(@NotNull final String id);

    @NotNull
    List<T> findAll();

    @NotNull
    T persist(@NotNull T entity) throws Exception;

    @NotNull
    T merge(@NotNull T entity);

    @Nullable
    T remove(@NotNull String id);

    void removeAll();

    @Nullable
    T findOne(@NotNull String id, @NotNull String userId) throws Exception;

    @Nullable
    List<T> findAll(@NotNull String userId) throws Exception;

    @NotNull
    T persist(@NotNull T object, @NotNull String userId) throws Exception;

    @Nullable
    T merge(@NotNull T entity, @NotNull String userId) throws Exception;

    @Nullable
    T remove(@NotNull String id, @NotNull String userId) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable
    List<T> findWord(@NotNull String word, @NotNull String userId);

    @NotNull
    T findOneByShortLink(@NotNull Integer shortLink, @NotNull String userId) throws Exception;
}
