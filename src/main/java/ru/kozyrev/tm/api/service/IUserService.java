package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {
    @Nullable
    User findOne(@Nullable String id) throws Exception;

    @Nullable
    List<User> findAll() throws Exception;

    @NotNull
    User persist(@Nullable User object) throws Exception;

    @Nullable User merge(@Nullable User object) throws Exception;

    @Nullable
    User remove(@Nullable String entityId) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User getUserByLogin(@Nullable String login) throws Exception;

    boolean isPasswordTrue(@Nullable String userLogin, @Nullable String hashPassword) throws Exception;
}
