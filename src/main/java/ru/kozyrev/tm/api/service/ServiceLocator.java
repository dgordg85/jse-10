package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.service.StateService;

public interface ServiceLocator {
    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    ITerminalService getTerminalService();

    @NotNull
    StateService getStateService();
}
