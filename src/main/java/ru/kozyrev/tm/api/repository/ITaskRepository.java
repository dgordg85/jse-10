package ru.kozyrev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.enumerated.Direction;

import java.util.List;

public interface ITaskRepository extends IAbstractObjectRepository<Task> {
    @Nullable
    Task findOne(@NotNull String id);

    @NotNull
    List<Task> findAll();

    @NotNull
    Task persist(@NotNull Task entity) throws Exception;

    @NotNull
    Task merge(@NotNull Task entity);

    @Nullable
    Task remove(@NotNull String id);

    void removeAll();

    @Nullable
    Task findOne(@NotNull String id, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAll(@NotNull String userId) throws Exception;

    @NotNull
    Task persist(@NotNull Task object, @NotNull String userId) throws Exception;

    @Nullable
    Task merge(@NotNull Task entity, @NotNull String userId) throws Exception;

    @Nullable
    Task remove(@NotNull String id, @NotNull String userId) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    @Nullable
    List<Task> findWord(@NotNull String word, @NotNull String userId);

    @NotNull
    Task findOneByShortLink(@NotNull Integer shortLink, @NotNull String userId) throws Exception;

    @Nullable
    List<Task> findAll(@NotNull String projectId, @NotNull String userId);

    void removeAll(@NotNull String projectId, @NotNull String userId);

    @Nullable
    List<Task> findAllByNum(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId);

    @Nullable
    List<Task> findAllByDateBegin(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId);

    @Nullable
    List<Task> findAllByDateFinish(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId);

    @Nullable
    List<Task> findAllByStatus(@NotNull String projectId, @NotNull Direction direction, @NotNull String userId);
}
