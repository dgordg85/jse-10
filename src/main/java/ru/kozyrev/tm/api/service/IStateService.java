package ru.kozyrev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.entity.User;

public interface IStateService {
    void resetState();

    boolean isUserAuth();

    @Nullable
    String getCurrentUserId();

    void clearUser();

    void setCurrentUser(@Nullable User user);
}
