package ru.kozyrev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.entity.User;
import ru.kozyrev.tm.enumerated.RoleType;

public final class UserRemoveCommand extends AbstractCommand {
    @NotNull
    public final static Integer SORT_ID = 11;

    public UserRemoveCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    @Override
    public final String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Use for deleting profile";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final User currentUser = serviceLocator.getStateService().getCurrentUser();

        System.out.println("[USER REMOVE]");
        System.out.println("Are you sure? Type 'yes' or another for cancel...");
        @NotNull final String answer = serviceLocator.getTerminalService().nextLine();
        if ("yes".equals(answer.toLowerCase())) {
            @Nullable final String userId = currentUser.getId();
            serviceLocator.getUserService().remove(userId);
            System.out.println("[USER DELETE!]");
            serviceLocator.getStateService().clearUser();
        } else {
            System.out.println("[OPERATION ABORT!]");
        }
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
