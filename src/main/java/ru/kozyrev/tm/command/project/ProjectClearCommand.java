package ru.kozyrev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kozyrev.tm.command.AbstractCommand;
import ru.kozyrev.tm.enumerated.RoleType;

public final class ProjectClearCommand extends AbstractCommand {
    public ProjectClearCommand() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
    }

    @NotNull
    public final static Integer SORT_ID = 28;

    @NotNull
    @Override
    public final String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public final String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public final void execute() throws Exception {
        @NotNull final String userId = serviceLocator.getStateService().getCurrentUserId();

        System.out.println("[CLEAR]");
        serviceLocator.getTaskService().removeAll(userId);
        System.out.println("[ALL TASKS REMOVED]");
        serviceLocator.getProjectService().removeAll(userId);
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    @NotNull
    @Override
    public final Integer getSortId() {
        return SORT_ID;
    }
}
